package br.ufrn.imd.praticaII;

import java.util.Calendar;

public class FuncionarioHandler {

	public static double calculaBonus(Funcionario funcionario) {
		double percentagemSobreVendas = 0.05;
		double percentagemSobreSalario = 0;
		double bonusDecimoTerceiro = 0;

		String cargo = funcionario.getCargo();

		switch (cargo) {
		case "gerente":
			percentagemSobreSalario = 0.1;
			break;
		case "diretor":
			percentagemSobreSalario = 0.2;
			bonusDecimoTerceiro = calculaBonificacaoDecimoTerceiro(funcionario);
			break;
		default:
			percentagemSobreVendas = 0.01;
			break;
		}

		double bonus = (funcionario.getVendasDoMes() * percentagemSobreVendas)
				+ (funcionario.getSalario() * percentagemSobreSalario) + bonusDecimoTerceiro;

		return bonus;
	}

	private static double calculaBonificacaoDecimoTerceiro(Funcionario funcionario) {
		int mesAtual = Calendar.getInstance().get(Calendar.MONTH);

		if (mesAtual == Calendar.DECEMBER) {
			return funcionario.getSalario();
		} else {
			return 0;
		}
	}
}
