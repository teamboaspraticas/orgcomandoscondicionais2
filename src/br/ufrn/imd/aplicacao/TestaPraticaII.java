package br.ufrn.imd.aplicacao;

import br.ufrn.imd.praticaII.Diretor;
import br.ufrn.imd.praticaII.Funcionario;
import br.ufrn.imd.praticaII.FuncionarioHandler;
import br.ufrn.imd.praticaII.Gerente;
import br.ufrn.imd.praticaII.Programador;

public class TestaPraticaII {

	public static void main(String[] args) {
		Funcionario programador;
		Funcionario gerente;
		Funcionario diretor;
			
		programador = new Programador();
		programador.setSalario(2000);
		programador.setVendasDoMes(10);
		
		gerente = new Gerente();
		gerente.setSalario(4000);
		gerente.setVendasDoMes(10);
		
		diretor = new Diretor();
		diretor.setSalario(10000);
		diretor.setVendasDoMes(10);
		
		System.out.println("Bonus do Programador: " + FuncionarioHandler.calculaBonus(programador));
		System.out.println("Bonus do Gerente: " + FuncionarioHandler.calculaBonus(gerente));
		System.out.println("Bonus do Diretor: " + FuncionarioHandler.calculaBonus(diretor));

	}

}
