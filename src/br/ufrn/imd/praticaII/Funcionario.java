package br.ufrn.imd.praticaII;

public abstract class Funcionario {
	private int vendasDoMes;
	private String cargo;
	private double salario;

	public Funcionario() {
	}
	
	public Funcionario(String cargo){
		this.cargo = cargo;
	}

	public int getVendasDoMes() {
		return vendasDoMes;
	}

	public void setVendasDoMes(int vendasDoMes) {
		this.vendasDoMes = vendasDoMes;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

}
